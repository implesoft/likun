package com.implesoft.lonalfa.rout;


import android.content.Context;
import android.graphics.Canvas;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

import com.implesoft.lonalfa.controller.GPSController;
import com.implesoft.lonalfa.controller.GeoPointController;

import org.osmdroid.ResourceProxy;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.Projection;
import org.osmdroid.views.overlay.Overlay;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nazar on 24.05.15.
 */
public class MyItemizedOverlay extends Overlay {
    Context context;
    GeoPoint p;
    GeoPoint loc;
    ArrayList<GeoPoint> geoPoints;
    FragmentActivity activity;
    GPSController gpsController;


    public MyItemizedOverlay(Context ctx, ArrayList<GeoPoint> geoPoints, FragmentActivity activity, GPSController gpsController) {
        super(ctx);
        this.gpsController = gpsController;
        context = ctx;
        this.geoPoints = geoPoints;
        this.activity = activity;
    }

    public MyItemizedOverlay(ResourceProxy pResourceProxy) {
        super(pResourceProxy);
    }

    @Override
    protected void draw(Canvas c, MapView osmv, boolean shadow) {

    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e, MapView mapView) {

        Projection proj = mapView.getProjection();
        p = (GeoPoint) proj.fromPixels((int) e.getX(), (int) e.getY());
        proj = mapView.getProjection();
        loc = (GeoPoint) proj.fromPixels((int) e.getX(), (int) e.getY());
        String longitude = Double
                .toString(((double) loc.getLongitudeE6()) / 1000000);
        String latitude = Double
                .toString(((double) loc.getLatitudeE6()) / 1000000);
        Toast toast = Toast.makeText(context,
                "Longitude: "
                        + longitude + " Latitude: " + latitude, Toast.LENGTH_SHORT);
        toast.show();
        mapView.getOverlays().clear();

        geoPoints = new ArrayList<>();
//        if (new GPSController(activity, mapView).isCanGetLocation()) {
        gpsController.showLocationMarker(gpsController.getLocationFromGPS());
//        }
        Location location = gpsController.getLocationFromGPS();
        if (location != null)
            geoPoints.add(new GeoPoint(location));

        GeoPointController.setGeoPoint(new GeoPoint(((double) loc.getLatitudeE6()) / 1000000,
                ((double) loc.getLongitudeE6()) / 1000000), mapView, activity);
        geoPoints.add(GeoPointController.getGeoPoint());
        Log.d("getLocationFromGPS", "context = " + context + " mapView = " + mapView + " geoPoints = " + new GeoPoint(location) +
                "getGeoPoint =  " + GeoPointController.getGeoPoint());

        new UpdateRoadTask(context, mapView).execute(geoPoints);

        return true;
    }

}
