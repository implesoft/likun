package com.implesoft.lonalfa.fragment;

import android.content.Context;
import android.graphics.Point;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.implesoft.lonalfa.R;
import com.implesoft.lonalfa.controller.GPSController;
import com.implesoft.lonalfa.controller.MarkerController;
import com.implesoft.lonalfa.controller.MusicController;

import org.osmdroid.api.IMapController;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

/**
 * Created by vika on 13.05.15.
 */
public class MainFragment extends Fragment implements View.OnClickListener {
    MapView mapView;
    IMapController controller;
    LocationManager manager;
    GPSController gpsController;
    ImageView btnRefresh;
    public static ImageView stopMusic;
    public static FragmentActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        init(view);
        // MarkerController.addPointToMap(48.8852, -24.728, MainFragment.this.getActivity(), mapView);
//        MarkerController.addPointToMap(50.4, -90.666667, MainFragment.this.getActivity(), mapView);

        return view;
    }


    private void init(View view) {

        manager = (LocationManager)
                getActivity().getSystemService(Context.LOCATION_SERVICE);

        mapView = (MapView) view.findViewById(R.id.map);
        btnRefresh = (ImageView) view.findViewById(R.id.btnRefreshLocation);
        mapView.setTileSource(TileSourceFactory.MAPNIK);
        mapView.setBuiltInZoomControls(true);
        mapView.setMultiTouchControls(true);
        mapView.setClickable(true);
        controller = mapView.getController();
        controller.setZoom(2);
        gpsController = new GPSController(getActivity(), mapView, manager);
        gpsController.showLocationMarker(gpsController.getLocationFromGPS());
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mapView.getOverlays().remove(MarkerController.anotherItemizedIconOverlay);
                mapView.refreshDrawableState();
                Location geoPoint = gpsController.getLocationFromGPS();
                if (gpsController.showLocationMarker(geoPoint)) {
                    controller.setZoom(30);
                    controller.animateTo(new GeoPoint(geoPoint));
                    mapView.invalidate();
                }
            }

        });
        btnRefresh.setOnClickListener(this);
        stopMusic = (ImageView) view.findViewById(R.id.stopMusic);
        stopMusic.setOnClickListener(this);
        LocationManager myLocationManager = (LocationManager) MainFragment.this.getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria crit = new Criteria();
        crit.setAccuracy(Criteria.ACCURACY_FINE);
        String best = myLocationManager.getBestProvider(crit, false);
        myLocationManager.requestLocationUpdates(best, 0, 1, new GPSController(MainFragment.this.getActivity(), mapView, manager));
        activity = getActivity();
    }

    @Override
    public void onDestroy() {

        if (MusicController.isplaying) {
            MusicController.stopMusic();
            stopMusic.setImageDrawable(getResources().getDrawable(R.drawable.stop));
        }
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.stopMusic:

                if (MusicController.isplaying) {
                    MusicController.stopMusic();
                    stopMusic.setImageDrawable(getResources().getDrawable(R.drawable.stop));
                } else {
                    MusicController.play("The_Hardkiss.mp3", activity);
                    stopMusic.setImageDrawable(getResources().getDrawable(R.drawable.play));

                }
                break;

            case R.id.btnRefreshLocation:

                if (new GPSController(getActivity(), mapView, manager).showLocationMarker(new GPSController(getActivity(), mapView, manager).getLocationFromGPS())) {
                    GeoPoint point = new GeoPoint(new GPSController(getActivity(), mapView, manager).getLocationFromGPS());
                    controller.setZoom((int) point.getLatitude());
                    controller.animateTo(point);
                }
                break;
        }
    }
}