package com.implesoft.lonalfa.controller;

import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

/**
 * Created by vika on 15.05.15.
 */
public class MarkerController {

    public static ItemizedIconOverlay<OverlayItem> anotherItemizedIconOverlay;

    /**
     * add Marker to MapView
     *
     * @param latitude
     * @param longitude
     * @param activity
     * @param mapView
     */
    public static void addPointToMap(double latitude, double longitude, FragmentActivity activity, MapView mapView) {
        ArrayList<OverlayItem> anotherOverlayItemArray = new ArrayList<OverlayItem>();
        anotherOverlayItemArray.add(new OverlayItem(
                "0, 0", "0, 0", new GeoPoint(latitude, longitude)));
        anotherItemizedIconOverlay
                = new ItemizedIconOverlay<>(
                activity, anotherOverlayItemArray, null);
        mapView.getOverlays().add(anotherItemizedIconOverlay);

    }

    public static void deletePointWithMap(MapView mapView) {

        mapView.getOverlays().remove(1);
        mapView.invalidate();

    }

}
