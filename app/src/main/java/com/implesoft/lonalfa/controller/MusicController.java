package com.implesoft.lonalfa.controller;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.view.View;

import com.implesoft.lonalfa.R;
import com.implesoft.lonalfa.fragment.MainFragment;

import java.io.IOException;

/**
 * Created by Domain on 15.05.2015.
 */
public class MusicController {

    static MediaPlayer mediaPlayer;
    public static boolean isplaying = false;

    public static void play(String path, Activity activity) {

        try {
            AssetFileDescriptor assetFileDescriptor = activity.getAssets().openFd(path);
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
            mediaPlayer.prepare();
            mediaPlayer.start();
            isplaying = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void stopMusic() {

        if(mediaPlayer != null && isplaying){

            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
            isplaying = false;

        }
    }
}