package com.implesoft.lonalfa.controller;

import android.graphics.Color;
import android.support.v4.app.FragmentActivity;

import com.implesoft.lonalfa.model.Marker;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.PathOverlay;

import java.util.List;

/**
 * Created by Yana on 13.05.2015.
 */
public class WayBuildingController {

    /**
     * Connects the coordinates on the map
     *
     * @param markers - list of the markers with certain coordinates;
     * @param activity - activity, wich`ll serve this method;
     * @param mapView - mapView, where`ll be built a way.
     */
    public void setWayMarkers(List<Marker> markers, FragmentActivity activity, MapView mapView) {

        if(markers.size() > 1) {
            PathOverlay pathOverlay = new PathOverlay(Color.RED, activity);

            for (Marker temp : markers) {

                GeoPoint geoPoint = new GeoPoint(temp.getLatitude(), temp.getLongitude());
                pathOverlay.addPoint(geoPoint);
            }

            mapView.getOverlays().add(pathOverlay);
        }
    }

    public double getLengthToMarker(Marker currentMarker, Marker marker){

//        Marker currentMarker = null;
        double cy = currentMarker.getLongitude();
        double cx = currentMarker.getLatitude();

        double my = marker.getLongitude();
        double mx = marker.getLatitude();

        double m1x = mx - cx;
        double m1y = my - cy;

        m1x = m1x * m1x;
        m1y = m1y * m1y;

        return Math.sqrt(m1x + m1y);
    }

}
