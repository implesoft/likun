package com.implesoft.lonalfa.controller;

import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;

import com.implesoft.lonalfa.R;
import com.implesoft.lonalfa.fragment.MainFragment;
import com.implesoft.lonalfa.rout.MyItemizedOverlay;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;

import java.util.ArrayList;

/**
 * Created by misha on 27.05.15.
 */
public class GPSController implements LocationListener {
    private Location location;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 2,
            MIN_TIME_BETWEEN_UPDATES = 1000;

    private static MyItemizedOverlay myItemizedOverlay;
    private static FragmentActivity activity;
    private static MapView mapView;

    private boolean isCanGetLocation = false;

    private double latitude;
    private double longitude;
    LocationManager locationManager;


    public GPSController(FragmentActivity activity, MapView mapView, LocationManager locationManager) {
        this.locationManager = locationManager;
        this.activity = activity;
        this.mapView = mapView;
    }

    public GPSController() {
    }

    /**
     * Get current location using GPS provider, doesn`t need access to network
     *
     * @return current location.
     */
    public Location getLocationFromGPS() {
        boolean isGPSEnabled = false;
        try {
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);


            if (!isGPSEnabled && !(InternetController.isMobile3G(activity) || InternetController.isWifi(activity))) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivity(intent);

            } else {
                isCanGetLocation = true;
                if (InternetController.isMobile3G(activity) || InternetController.isWifi(activity)) {
                    locationManager.removeUpdates(this);
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BETWEEN_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES
                            , this);

                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = getLatitude();
                            longitude = getLongitude();
                        }

                    }
                } else {
                    if (isGPSEnabled) {
                        locationManager.removeUpdates(this);
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                                MIN_TIME_BETWEEN_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
        } catch (
                Exception e
                )

        {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * method get Latitude
     *
     * @return latitude
     */

    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }
        return latitude;
    }

    /**
     * method get Longitude
     *
     * @return longitude
     */

    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }
        return longitude;
    }

    /**
     * method check GPS/WI-FI enable
     *
     * @return
     */
    public boolean isCanGetLocation() {
        return this.isCanGetLocation;
    }

    /**
     * Draw a marker for certain location
     *
     * @param location - location, which should be marked on the map
     */

    public boolean showLocationMarker(Location location) {
        if (location != null) {
            Log.d("getLocationFromGPS", "showLocationMarker");

            mapView.getOverlays().remove(MarkerController.anotherItemizedIconOverlay);
            mapView.refreshDrawableState();

            MarkerController.addPointToMap(location.getLatitude(), location.getLongitude(), activity, mapView);
            ArrayList<GeoPoint> geoPoints = new ArrayList<>();
            geoPoints.add(new GeoPoint(location.getLatitude(), location.getLongitude()));
            myItemizedOverlay = new MyItemizedOverlay(activity, geoPoints, activity, this);
            mapView.getOverlays().add(myItemizedOverlay);

            mapView.getController().setZoom(mapView.getZoomLevel());
            mapView.getController().animateTo(new GeoPoint(location.getLatitude(), location.getLongitude()));

            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.d("getLocationFromGPS", "onLocationChanged");
        showLocationMarker(location);
        if (MarkerController.anotherItemizedIconOverlay != null && GeoPointController.calculateDistance
                (location, MarkerController.anotherItemizedIconOverlay.getItem(MarkerController.anotherItemizedIconOverlay.size() - 1)
                        .getPoint()) < 2 && !MusicController.isplaying) {

            MusicController.play("The_Hardkiss.mp3", activity);
            MarkerController.deletePointWithMap(mapView);
            MainFragment.stopMusic.setVisibility(View.VISIBLE);
            Log.d("music", "start playing music");
        } else if (MarkerController.anotherItemizedIconOverlay != null && GeoPointController.calculateDistance
                (location, MarkerController.anotherItemizedIconOverlay.getItem(MarkerController.anotherItemizedIconOverlay.size() - 1)
                        .getPoint()) < 3 && MusicController.isplaying) {

            MusicController.stopMusic();
            MainFragment.stopMusic.setImageDrawable(MainFragment.activity.getResources().getDrawable(R.drawable.play));
            MainFragment.stopMusic.setVisibility(View.GONE);
            Log.d("music", "stop playing music");
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        getLocationFromGPS();
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {
        getLocationFromGPS();
    }

}
