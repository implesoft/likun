package com.implesoft.lonalfa.activity;


import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;

import com.implesoft.lonalfa.R;
import com.implesoft.lonalfa.controller.WayBuildingController;
import com.implesoft.lonalfa.fragment.MainFragment;
import com.implesoft.lonalfa.model.Marker;

import org.osmdroid.util.GeoPoint;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            Log.d("MyLog", "result = " + new WayBuildingController().getLengthToMarker(new Marker(3, 3), new Marker(1, 5)));
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new MainFragment(), "MainFragment")
                    .commit();
        }

    }


}

